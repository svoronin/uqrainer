package ua.pp.voronin.serhii.softserve.graphql.uqrainer.datasource;

import org.springframework.stereotype.Component;
import ua.pp.voronin.serhii.softserve.graphql.uqrainer.model.Article;
import ua.pp.voronin.serhii.softserve.graphql.uqrainer.model.Author;
import ua.pp.voronin.serhii.softserve.graphql.uqrainer.model.User;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

@Component
public class InMemoryStructure {

    private final Author auth1 = new Author(1L, "Anton", "Pysarenko", new URL("http://my-page.net/AP"));
    private final Article art1 = new Article(1L, "Ternopil Overview", "Some insight into local life", auth1);
    private final Article art2 = new Article(2L, "Kam`yanets-Podilskyi Overview", "Some insight into local life", auth1);
    private final Article art3 = new Article(3L, "Chernivtsi Overview", "Some insight into local life", auth1);

    private final Author auth2 = new Author(2L, "Borys", "Tvorchiy", new URL("http://arts-and-artists.info/profile/BT"));
    private final Article art4 = new Article(4L, "Kherson Overview", "Some insight into local life", auth2);
    private final Article art5 = new Article(5L, "Mykolayiv Overview", "Some insight into local life", auth2);
    private final Article art6 = new Article(6L, "Skadovsk Overview", "Some insight into local life", auth2);

    private final User u1 = new User(1L, "anastasiia@email.com", "Anastasiia", "Korystuvatska", List.of(art1));
    private final User u2 = new User(2L, "barbara@inbox.net", "Barbara", "Schmidt", List.of(art3, art4));
    private final User u3 = new User(3L, "vsevolod@official.ua", "Vsevolod", "Obyrayko", List.of(art1, art2, art3, art4, art5, art6));

    private final List<Article> articles = List.of(art1, art2, art3, art4, art5, art6);
    private final List<Author> authors = List.of(auth1, auth2);
    private final List<User> users = List.of(u1, u2, u3);

    public InMemoryStructure() throws MalformedURLException {}

    public List<Article> getArticles() {
        return articles;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public List<User> getUsers() {
        return users;
    }
}
