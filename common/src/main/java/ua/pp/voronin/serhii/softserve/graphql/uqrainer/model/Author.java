package ua.pp.voronin.serhii.softserve.graphql.uqrainer.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.net.URL;

@AllArgsConstructor
@Data
public class Author {
    private Long id;
    private String lastName;
    private String firstName;
    private URL profilePage;
}
