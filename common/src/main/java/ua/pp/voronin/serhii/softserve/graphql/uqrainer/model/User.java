package ua.pp.voronin.serhii.softserve.graphql.uqrainer.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@AllArgsConstructor
@Data
public class User {
    private Long id;
    private String email;
    private String lastName;
    private String firstName;
    private List<Article> favourites;
}
