package ua.pp.voronin.serhii.softserve.graphql.uqrainer.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Article {
    private Long id;
    private String title;
    private String body;
    private Author author;
}
