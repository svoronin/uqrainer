package ua.pp.voronin.serhii.softserve.graphql.uqrainer.service;

import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.pp.voronin.serhii.softserve.graphql.uqrainer.datasource.InMemoryStructure;
import ua.pp.voronin.serhii.softserve.graphql.uqrainer.model.User;

import java.util.Optional;

@Service
@GraphQLApi
public class UserService {

    @Autowired
    private InMemoryStructure source;

    @GraphQLQuery(name = "user")
    public Optional<User> getUserById(@GraphQLArgument(name = "id") Long id) {
        return source.getUsers()
                .stream()
                .filter(u -> id.equals(u.getId()))
                .findAny();
    }
}
